#!/usr/bin/env python
# coding: utf-8
from gensim.models import FastText, Word2Vec, translation_matrix, KeyedVectors
import itertools
import logging
import numpy as np
import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity as cosine
from scipy.stats import spearmanr

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

langs = ['en', 'it', 'fr']

def read_input(filename):
    with open(filename, "r", encoding='utf-8') as f:
        file_lines = f.read().splitlines()
    for line in file_lines:
        yield [word for word in line.split(" ") if word]


# Grid search : creating combinations of params
params = {'epochs':[5,10], 'size': [200, 300, 400], 'window': [5, 10], 'min_count': [1, 2, 3], 'negative': [5, 10]}
combinations = itertools.product(*(params[param] for param in params.keys()))

grid = []
for combination in combinations:
    row = {'workers': 10, 'sg': 0, 'hs': 1}
    row['epochs'] = combination[0]
    row['size'] = combination[1]
    row['window'] = combination[2]
    row['min_count'] = combination[3]
    row['negative'] = combination[4]
    grid.append(row)

# Read the corpuses
documents = {lang: list(read_input("resources/corpus_" + lang + ".txt")) for lang in langs}
logging.info("Done reading corpuses")

# Read gold set
with open('../resources/combined.tab', 'r') as f:
    lines = [x.strip().split('\t') for x in f]
del lines[0]  # delete header


# Compute the common synsets across languages
def compute_common_synsets(models):
    lang_synsets = {lang: {} for lang in models.keys()}
    for lang in models:
        lang_synsets[lang] = set([word.split("_bn:")[1] for word in models[lang].wv.vocab.keys() if '_bn:' in word])
    # Return only the common elements
    return list(set(list(lang_synsets['en'])).intersection(*lang_synsets.values()))


# Dictionary of synsets for each lang
def compute_synset_dicts(models, common_synsets):
    lang_synsets = {lang: {} for lang in models.keys()}
    for lang in models:
        for word in models[lang].wv.vocab.keys():
            if '_bn:' not in word:
                continue
            synset = word.split("_bn:")[1]
            if synset in common_synsets:
                if synset not in lang_synsets[lang].keys():
                    lang_synsets[lang][synset] = [word]
                else:
                    lang_synsets[lang][synset].append(word)
    return lang_synsets


# Compute the word pairs for english vs others languages
def compute_word_pairs(synsets_dict):
    word_pairs = {}
    for lang in langs:
        if lang == 'en': continue
        if 'en_' + lang not in word_pairs.keys():
            word_pairs['en_' + lang] = []
        for synset in common_synsets:
            [word_pairs['en_' + lang].append(tuple((x, y))) for x in synsets_dict[lang][synset] for y in synsets_dict['en'][synset]]
    return word_pairs


# Compute translation matrices eng vs other language:
def compute_translation_matrices(word_pairs, models):
    translation_matrices = {}
    for pairs in word_pairs:
        translation_matrices[pairs] = translation_matrix.TranslationMatrix(models[pairs.split("_")[1]].wv, models['en'].wv, word_pairs[pairs])
        translation_matrices[pairs].train(word_pairs[pairs])
    return translation_matrices


# Compute the centroid of a synsets
def find_centroid(word_vecs, model=None, transmat=None):
    if transmat is not None:
        word_vecs = [transmat.translation_matrix.dot(model.wv[vector]).reshape(1, -1) for vector in word_vecs]
    elif model is not None:
        word_vecs = [model.wv[word] for word in word_vecs]
    return np.mean(word_vecs, axis=0)


# Compute the new english space moving synsets vectors alongside their cross-language centroid
def compute_translated_sense_embeddings(synsets_dict, translation_matrices, models):
    translated_sense_embeddings = {}
    alpha = 0.5
    for synset in common_synsets:
        # Compute the centroids for each language
        centroids = {lang: find_centroid(synsets_dict[lang][synset], models[lang], translation_matrices['en_' + lang])[0] for lang in langs if lang != 'en'}
        # Since english is our target space, we don't need use translation matrix
        centroids['en'] = find_centroid(synsets_dict['en'][synset], models['en'])
        # Compute the centroid of the centroids
        centroid = find_centroid(list(centroids.values()))

        for lang in langs:
            # Find the direction from the language synset centroid to the "center" centroid
            direction = centroid - centroids[lang]
            for word_sense in synsets_dict[lang][synset]:
                if lang != 'en':
                    vector = translation_matrices['en_' + lang].translation_matrix.dot(models[lang][word_sense]).reshape(1, -1)[0]
                else:
                    vector = models['en'][word_sense].reshape(1, -1)[0]
                # Move each word sense vector alongside the direction of the "center" centroid
                translated_sense_embeddings[word_sense] = np.array(vector + alpha * direction)
                del vector
    return translated_sense_embeddings


def compute_score(model, model_type):
    if model_type == 'translated':
        vocab = model.keys()
    else:
        vocab = model.wv.vocab.keys()
        model = model.wv
    lines_updated = []
    for words in lines:
        # Computing the list of words regarding
        s1 = [token for token in vocab if
              (token.split("_")[0] == words[0].lower() and token.count("_") <= 1)]

        s2 = [token for token in vocab if
              (token.split("_")[0] == words[1].lower() and token.count("_") <= 1)]

        score = -1.0
        for pair in itertools.product(s1, s2):
            try:
                score = max(score, cosine(model[pair[0]], model[pair[1]]))
            except ValueError:
                score = max(score, cosine(model[pair[0]].reshape(1, -1), model[pair[1]].reshape(1, -1)))
        lines_updated.append([words[0], words[1], words[2], score])

    gold_values = [float(el[2]) for el in lines_updated]
    # For translation score
    cosine_values = [el[3] for el in lines_updated]
    corr, p_value = spearmanr(gold_values, cosine_values)
    return corr, p_value


def dump_sense_embeddings(model, filename, model_type):
    if model_type == 'translated':
        vocab = model.keys()
    else:
        vocab = model.wv.vocab.keys()
    with open(filename, 'w+') as f:
        sense_embeddings = [word for word in vocab if "_bn:" in word]
        f.write(str(len(sense_embeddings)) + " " + str(model[sense_embeddings[0]].shape[0]) + "\n")
        for sense in sense_embeddings:
            f.write(sense + " ")
            for dim in model[sense]:
                f.write(str(dim) + " ")
            f.write("\n")
    f.close()


if __name__ == '__main__':
    # Training
    results = {'epochs': [], 'size': [], 'window': [],
                    'min_count': [], 'negative': [],
                    'corr': [], 'p_value': [],
                    'corr_trans': [], 'p_value_trans': []}

    # Compute the models using grid search paradigm
    for row in grid:
        logging.info("Training models......")
        # Instantiate, train and save models for each language
        models = {}
        for lang in langs:
            model = Word2Vec(documents[lang], size=row['size'], window=row['window'],
                             min_count=row['min_count'], workers=row['workers'],
                             negative=row['negative'], sg=row['sg'], hs=row['hs'])
            model.train(documents[lang], total_examples=len(documents[lang]), epochs=row['epochs'])

            models[lang] = model
            del model

        logging.info("Models computed")

        # --- REGULAR APPROACH ---
        # Score
        corr, p_value = compute_score(models['en'], 'Word2Vec')
        logging.info("Translation model score: corr = "+str(corr)+" p_value_trans = "+str(p_value))

        results['epochs'].append(row['epochs'])
        results['min_count'].append(row['min_count'])
        results['negative'].append(row['negative'])
        results['size'].append(row['size'])
        results['window'].append(row['window'])
        results['corr'].append(corr)
        results['p_value'].append(p_value)

        filename = "resources/embeddings_en_epochs="+str(row['epochs'])+"_min_count="+str(row['min_count'])\
                   +"_negative="+str(row['negative'])+"_size="+str(row['size'])+"_window="+str(row['window'])+"_.vec",
        dump_sense_embeddings(models['en'], filename)
        # --- CUSTOM APPROACH ---

        # Compute common synsets for each language
        common_synsets = compute_common_synsets(models)
        logging.info("Common synsets computed")

        # Group each word sense to their synset for each language
        synsets_dict = compute_synset_dicts(models, common_synsets)
        logging.info("Synsets dict computed")

        # Compute the word pairs (eng, other language) for computing translation matrix
        word_pairs = compute_word_pairs(synsets_dict)
        # es results : ('cortile_bn:00024541n', 'ground_bn:00024541n'), ('cortile_bn:00024541n', 'yard_bn:00024541n'),
        #           ('giardino_bn:00024541n', 'ground_bn:00024541n'), ('giardino_bn:00024541n', 'yard_bn:00024541n')...
        logging.info("Word pairs computed")

        # Compute translation matrices
        translation_matrices = compute_translation_matrices(word_pairs, models)
        logging.info("Translation matrices computed")

        # Compute the new vector space
        translated_sense_embeddings = compute_translated_sense_embeddings(synsets_dict, translation_matrices, models)
        # Add vectors which don't belongs to the cross language common synsets
        vocab_diff = list(set(models['en'].wv.vocab) - set(synsets_dict['en'].keys()))
        for word in vocab_diff:
            translated_sense_embeddings[word] = models['en'][word].reshape(1, -1)[0]

        # Score
        corr_trans, p_value_trans = compute_score(translated_sense_embeddings, 'translated')
        print("Translation model score: corr = ", corr_trans, " p_value_trans = ", p_value_trans)

        results['corr_trans'].append(corr_trans)
        results['p_value_trans'].append(p_value_trans)

        del models
        del translated_sense_embeddings
        del word_pairs
        del synsets_dict
        del common_synsets

    df = pd.DataFrame(results, columns=list(results.keys()))
    df.to_csv('results.csv', encoding='utf-8', sep='\t')
