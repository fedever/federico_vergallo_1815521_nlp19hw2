from lxml import etree
import nltk
import string
import urllib.request as urllib2
import html
import logging

logging.basicConfig(filename='resource_logger.log',
                    format='%(asctime)s : %(levelname)s : %(message)s',
                    level=logging.INFO,
                    filemode='w')

punct = string.punctuation
punct = punct[:punct.index("_")] + punct[punct.index("_")+1:]
punct = punct[:punct.index(":")] + punct[punct.index(":")+1:]

filename = 'EuroSense/eurosense.v1.0.high-coverage.xml'

context = etree.iterparse(filename, tag='sentence')

bn2wn_file = 'resources/bn2wn_mapping.txt'
bn_synsets = [x.split('\t')[0] for x in open(bn2wn_file).readlines()]


stp_words = {'it': ['l', 'c', 'dell', 'di', 'a', 'da', 'in', 'con', 'su', 'per', 
             'tra', 'fra', 'il', 'la', 'le', 'lo', 'gli', 'degl', 'cosa', 'come',
             'perchè', 'quando', 'dove', 'se', 'al', 'alle', 'là',
             'che', 'come', 'nell', 'nel', 'della', 'nella', 'a', 'e', 'i',
             'o', 'un', 'ad', 'alla', 'alla', 'dalle', 'delle', 'degli', 'dall',
             'una', 'del', 'dai', 'dal', 'd', ':' , '_'],
             
            'en': ['from', 'the', 'with', 'why','because', 'at', 'to', 'into', 'onto'
            'for', 'while', 'where', 'what', 'when', 'if', 'and', 'of', ':' , '_'],
             
            'fr': ['ce',  'ci',  'du', 'en', 'et', 'il', 'ils',  'la', 'le',
                   'les', 'là', 'ou', 'que', 'quel', 'sa', 'si', 'ta', 'tu', 'vu','ça',
                  'l', 'j', 'de', 'm']
            }

def preprocess(text, lang):
    if type(text) == str: 
        text = urllib2.unquote(text)
        text = html.unescape(text)
        text = text.translate(str.maketrans('', '', punct)).lower().split(" ")
        text = [word for word in text if word not in stp_words[lang]]
        return [word for word in text if word]
    else:
        return False


langs = ['it', 'en', 'fr']
# Storing id of sentence for each lang (just for printing)
sent_lang_id = {lang:1 for lang in langs}
for event, sentence in context:
    '''
    The approach is:
        1 - if the sentence is translated in a language within langs, then:
                1 - preprocess text 
                2 - create bigrams
                
        2 - after processing the text tags, if the sentence is translated into 
            ALL language within langs, then: 
                1 - substitute the annotated anchor with correspondent lemma_synset
                2 - if annotation is a bigram, then find the unigram that compose
                    the bigram and insert (bigram)lemma_synset between those unigrams
                    
        3 - after processing the annotations tags, if the sentence is translated into 
            ALL language within langs, then:
                1 - write the line in the corresponding corpus txt file of each
                    language
    '''
    
    # I will save each translated sentence
    texts = {lang:'' for lang in langs}
    # Backup dictionary variable 
    original_texts = {lang:'' for lang in langs}
    # Storing bigrams for each lang
    texts_bigrams = {lang:[] for lang in langs}
    
    
    for sent in sentence:
        # We are in language Sentence tags
        if 'lang' in list(dict(sent.attrib).keys()):
            if sent.attrib['lang'] not in langs:
                continue
            else:
                # Preprocess: remove punctuation and tokenize
                try:
                    preprocessed_text = preprocess(sent.text, sent.attrib['lang'])
                    if type(preprocessed_text) is bool:
                        continue
                    texts[sent.attrib['lang']] = preprocessed_text
                    original_texts[sent.attrib['lang']] = texts[sent.attrib['lang']].copy()
                except AttributeError:
                    continue
                # Create bigrams
                bigrams = list(nltk.bigrams(texts[sent.attrib['lang']]))
                texts_bigrams[sent.attrib['lang']] = [bigram[0]+" "+bigram[1] for bigram in bigrams]
                sent_lang_id[sent.attrib['lang']] = sentence.attrib['id']
                continue

        #We are in annotations tags
        if sent.tag == 'annotations':
            for annotation in sent:
                if annotation.text in bn_synsets and annotation.attrib['lang'] in langs:
                    try:
                        if annotation.attrib['anchor'] in texts_bigrams[annotation.attrib['lang']]:
                            # Find the indeces of unigram composing the bigram
                            indices_unigram = [i for i, s in enumerate(original_texts[annotation.attrib['lang']]) if annotation.attrib['anchor'].split(" ")[0] in s]
                            # Put bigram between the unigrams
                            for index in indices_unigram:
                                texts[annotation.attrib['lang']].insert(index+1, "_".join(annotation.attrib['lemma'].split(" "))+"_"+annotation.text)

                        elif annotation.attrib['anchor'] in texts[annotation.attrib['lang']]:
                            indeces = [n for (n, e) in enumerate(texts[annotation.attrib['lang']]) if e == annotation.attrib['anchor']]
                            for index in indeces:
                                texts[annotation.attrib['lang']][index] = annotation.attrib['lemma']+"_"+annotation.text
                        elif annotation.attrib['anchor']+"_" in texts[annotation.attrib['lang']]:
                            indeces = [n for (n, e) in enumerate(texts[annotation.attrib['lang']]) if e == annotation.attrib['anchor']+"_"]
                            for index in indeces:
                                texts[annotation.attrib['lang']][index] = annotation.attrib['lemma']+"_"+annotation.text
                    except Exception as e:
                        print(e)
                        print(annotation.attrib)
                        print(texts)
                        print(bigrams)
                        print("---"*5)
                        
    message = 'Last id sentence for lang: '
    for lang in langs:
        message += lang + " " + str(sent_lang_id[lang]) + " "
    logging.info(message)
    sentence.clear()
    for lang in langs:
        with open("resources/corpus_"+lang+".txt", "a", encoding='utf-8') as f:
            text = ' '.join(texts[lang])
            f.write(text)
            f.write("\n")

